# Reference: <https://postmarketos.org/vendorkernel>
# Maintainer:  Clayton Craft <clayton@craftyguy.net>
pkgname=linux-purism-librem5
# Note: _p#, where # is the purism version, e.g. the '2' in 'librem5.2'
pkgver=5.7.9_p1
pkgrel=0
_kernver=${pkgver%_p*}
_purismrel=${pkgver#*_p}
# <kernel ver>.<purism kernel release>
_purismver=${_kernver}+librem5.$_purismrel
pkgdesc="Purism Librem 5 phone kernel fork"
arch="aarch64"
_carch="arm64"
_flavor="purism-librem5"
url="https://source.puri.sm/Librem5/linux-next"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev
	     devicepkg-dev bison flex openssl-dev"

# Source
_repository="linux-next"
# kconfig generated with: ARCH=arm64 make defconfig KBUILD_DEFCONFIG=librem5_defconfig
_config="config-$_flavor.$arch"
source="
	$pkgname-$_purismver.tar.gz::https://source.puri.sm/Librem5/$_repository/-/archive/pureos/$_purismver/$_repository-pureos-$_purismver.tar.gz
	$_config
"
builddir="$srcdir/$_repository-pureos-$_purismver"

prepare() {
	default_prepare
	REPLACE_GCCH=0 \
		. downstreamkernel_prepare
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS" \
		LOCALVERSION=".$_purismrel"
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"

}
sha512sums="4a772d0745437838580b904d99469f2e3a762d8af7e7431dd685f890e6bdb47a8f8da36ea4524de346716f325f283079da2e1d4d607c9e19e3a3a6aaf6b5c478  linux-purism-librem5-5.7.9+librem5.1.tar.gz
5824f9c64efe8c6b9b25d6ead0819077d6091b576692247a46bcfbac856f56b309e94917cd175a86b8e4e085b480bb783b3a7c787203352643259ff2c3193290  config-purism-librem5.aarch64"
